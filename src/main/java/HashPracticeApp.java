import org.HashFuncs.MultiplicationHashFunction;
import org.HashTables.SimpleHashTable;
import org.HashFuncs.HashFunctionType;

/**
 * Created by lucaspedroza on 3/6/16.
 */
public class HashPracticeApp {

    public static void main(String[] args) {
        RunHash();
    }

    public static void RunHash() {
        int hashTableSize = 1033;
        SimpleHashTable ht = new SimpleHashTable(hashTableSize);

        long intHashResult;
        long stringHashResult;
        int ints[] = { 1,2,3,4,5,6,10,12,435,346,674,84675,234};

        for (int i : ints) {
            intHashResult = MultiplicationHashFunction.crunch(i);
            System.err.printf("Multiplication Hash Function on Integers: %d -> %d%n",i,intHashResult);
            ht.insert(intHashResult,i);
        }

        String strings[] = { "Lucas","Brianna","Thea", "Mathew", "Darius", "Cray", "Lucas"};

        for (String s : strings) {
            stringHashResult = MultiplicationHashFunction.crunch(s);
            System.err.printf("Multiplication Hash Function on Strings: %s -> %d%n",s,stringHashResult);
            ht.insert(stringHashResult,s);
        }

        double newHash = MultiplicationHashFunction.crunch("Lucas");
        Object result = ht.search(newHash,"Lucas");
        result = ht.search(newHash,"Mathew");
        result = ht.search("Brianna");
        result = ht.delete(newHash, "Lucas");
    }
}
