package org.HashTables;
import java.util.ArrayList;

/**
 * Created by lucaspedroza on 3/6/16.
 */


public class SimpleHashTable {
    private ArrayList<ArrayList<Object>> ht;

    public SimpleHashTable(int hashTableSize) {
        this.ht = new ArrayList<ArrayList<Object>>(hashTableSize);
        for (int i = 0; i < hashTableSize; i++) {
            this.ht.add(new ArrayList<Object>(1));
        }
    }

    public void insert(double hash, Object value) {
        int index = NormalizeHash(hash);
        ArrayList<Object> currentArray = this.ht.get(index);
        if (currentArray.isEmpty()){
            currentArray.ensureCapacity(1);
        } else {
            currentArray.ensureCapacity(currentArray.size()+1);
        }
        this.ht.get(index).add(value);
        System.err.printf("Added %s to index (%d, %d)!%n",value,index,this.ht.get(index).size()-1);
    }

    // Deletes the first occurrence of of the value passed at the index passed
    public boolean delete(double hash, Object value) {
        int index = NormalizeHash(hash);
        ArrayList<Object> currentArray = this.ht.get(index);
        if (currentArray.isEmpty()) {
            System.err.printf("Nothing to delete at index %d!%n", index);
        }
        else {
            for (int i = 0; i < currentArray.size(); i++){
                if (currentArray.get(i) == value) {
                    currentArray.remove(i);
                    System.err.printf("Deleted %s from index (%d, %d).%n",value, index, i);
                    return true;
                }
            }
        }
        System.err.printf("Value %s not found at index %d!%n", value, index);
        return false;
    }

    public boolean search(double hash, Object value) {
        int index = NormalizeHash(hash);
        ArrayList<Object> currentArray = this.ht.get(index);
        for (int i = 0; i < currentArray.size(); i++) {
            if (currentArray.get(i) == value) {
                System.err.printf("Found value %s at index (%d, %d)!%n", value, index, i);
                return true;
            }
        }
        System.err.printf("Value %s not found at index %d!%n", value, index);
        return false;
    }

    public boolean search(Object value) {
        for (int i = 0; i < this.ht.size(); i++) {
            ArrayList<Object> currentArray = this.ht.get(i);
            for (int j = 0; j < currentArray.size(); j++) {
                if (currentArray.get(j) == value) {
                    System.err.printf("Found value %s at index (%d, %d)!%n", value, i, j);
                    return true;
                }
            }
        }
        System.err.printf("Value %s not found!%n", value);
        return false;
    }

    private int NormalizeHash(double hash) {
        if(hash > ht.size()) {
            return (int) (hash % ht.size());
        }
        else if (hash >= 0.0 & hash <= 1.0) {
            return (int) (hash * ht.size());
        }
        else if (hash < 0.0)
            return 0;
        else {
            return (int) hash;
        }

    }

}
