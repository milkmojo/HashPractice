package org.HashFuncs;


/**
 * Created by lucaspedroza on 3/6/16.
 */
public class MultiplicationHashFunction {
    private static final double factor = Math.PI; //some irrational number

    public static long crunch(int key) {
        long hash = (long) Math.pow(key*factor,2);
        return hash;
    }

    public static long crunch(String key) {
        char chars[] = key.toCharArray();
        int alpha = 26;
        long hash = 0;
        for (int i = 0; i < chars.length; i++) {
            hash += (long) Math.pow(alpha,chars.length - i + 1) * chars[i];
        }
        return hash;
    }


}
