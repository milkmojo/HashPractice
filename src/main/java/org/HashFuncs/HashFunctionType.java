package org.HashFuncs;

/**
 * Created by lucaspedroza on 3/8/16.
 */
public enum HashFunctionType {
    Multiplication,
    Division
}
